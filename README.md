# Install Codiad on Termux

clone this repo into your termux home. 

run `sh install_codiad.sh` from temux; should install to `~/codiad/`

replace the php script in `~/codiad/lib/diff_match_patch.php` with the one in this repo

run `php -S 127.0.0:9876 & xdg-open http://127.0.0.1:9876` to start running Codiad